<?php

use app\models\Etapa;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Etapas';

// Registrando ScrollReveal en la vista
/* 
$this->registerJs("
    ScrollReveal({
        reset: true,
        distance: '0px',
        duration: 2500,
        delay: 400
    });

    ScrollReveal().reveal('.anim-c', { delay: 500, origin: 'bottom', interval: 200});
");*/
?>

<div class="site-index section-etapas">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Puertos</h1>
    </div>


    <div class="body-content n-root wrapper">
        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Coll de Espina</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $puerto1,
                            'columns' => $campos1,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>
        
        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Coll de Montaup</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $puerto2,
                            'columns' => $campos2,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>
        
        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Coll de sa Batalla</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $puerto3,
                            'columns' => $campos3,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>
        
        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Estación de Esquí de Cerler</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $puerto4,
                            'columns' => $campos4,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>
        
        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Laguna Negra de Niela</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $puerto5,
                            'columns' => $campos5,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>
        
        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Puerto de Albaida</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $puerto6,
                            'columns' => $campos6,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>
        
        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Puerto de Cotos</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $puerto7,
                            'columns' => $campos7,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>
        
        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Puerto de Cotos</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $puerto8,
                            'columns' => $campos8,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>
        
        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Puerto de la Cruz de Perves</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $puerto9,
                            'columns' => $campos9,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>
        
        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Puerto de la Cruz de Perves</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $puerto10,
                            'columns' => $campos10,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>
        
        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Puerto de la Morcuera</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $puerto11,
                            'columns' => $campos11,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>
        
        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Puerto de León</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $puerto12,
                            'columns' => $campos12,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>
        
        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Puerto de Navacerrada</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $puerto13,
                            'columns' => $campos13,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>
        
        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Puerto de Pedro Bernardo</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $puerto14,
                            'columns' => $campos14,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>
        
        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Puerto de Tibi</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $puerto15,
                            'columns' => $campos15,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Sierra Nevada</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $puerto16,
                            'columns' => $campos16,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

    </div>
</div>