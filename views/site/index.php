<?php

/** @var yii\web\View $this */

use yii\helpers\Html;


$this->title = 'La Vuelta Al Pasado';

$this->registerJs("
    ScrollReveal({
        reset: true,
        distance: '0px',
        duration: 2500,
        delay: 400
    });

    ScrollReveal().reveal('.title.subtitle, .text-i, ciclista-img', { delay: 500, origin: 'top'});
");


?>
<div class="container-h">

    <div class="home contenedor-l">

        <div class="row p-0 m-0">
            <div class="s-1">
                <div class="info col-3">
                    <div class="titles">
                        <h1 class="titulo-h titulo animate__animated animate__fadeIn">Ciclismo</h1>
                        <h2 class=" subtitulo-h subtitulo animate__animated animate__fadeIn">La Vuelta Al Pasado</h2>
                    </div>

                    <div class="info-principal">
                        <h3 class="subtitle-l-v">Bienvenido a 'La Vuelta Al Pasado'!</h3>
                        <?= \yii\helpers\Html::a('Ver Equipos <i class="fa-solid fa-arrow-right"></i>', ['equipo/index'], ['class' => 'btn btn-c']) ?>
                    </div>
                </div>

                <!-- IMAGEN CICLISTA -->
                <div class="img-ciclista col-6">
                    <img src="<?= Yii::$app->request->baseUrl ?>/img/bicicleta.webp" alt="Cilista" class="ciclista animate__animated animate__fadeInRight">
                </div>

                <div class="c-info">
                    <div class="cont-section serv">
                        <nav class="list-services" data-list_name="list">
                            <?= Html::a(
                                Html::decode(
                                    '
                    <div class="menu-card-bx-top">
                        <span class="mod-svg id_svg_1143 nav-icon-prin"><svg preserveAspectRatio="xMidYMid meet" viewBox="0 0 48 48"><!-- Contenido del icono --></svg></span>
                        <span class="nav-title">Clasificación</span>
                        <span class="nav-sub_title">.</span>
                    </div>
                    <div class="menu-card-bx-down">
                        <span class="mod-svg id_svg_1143 nav-icon-prin"><svg preserveAspectRatio="xMidYMid meet" viewBox="0 0 48 48"><!-- Contenido del icono --></svg></span>
                        <span class="nav-title">Ciclistas</span>
                        <span class="nav-sub_title">Información detallada sobre la Clasificación en el sistema.</span>
                    </div>'
                                ),
                                ['/clasificacion/index'],
                                ['class' => 'nav-a text-white btn']
                            ) ?>
                        </nav>
                    </div>
                </div>

            </div>


        </div>

        <div class="row m-0 d-flex justify-content-center s-2">
            <!-- SECCIÓN 2-->
            <div class="col-4 p-0 ciclista-img">
                <img src="<?= Yii::$app->request->baseUrl ?>/img/bicicleta2.png" alt="Cilista" class="ciclista ">
            </div>
            <div class="col-5 s2-info ">
                <h2 class="title subtitle">Acerca de</h2>
                <p class="text-i">¡Bienvenido a 'La Vuelta Al Pasado'! Tu destino principal para descubrir información detallada sobre ciclistas de renombre y prometedores. Sumérgete en perfiles completos que destacan las hazañas, logros y trayectorias de los ciclistas más destacados de todos los tiempos.</p>
                <p class="text-i">Explora datos biográficos, estadísticas de carrera y momentos emblemáticos que han definido la historia del ciclismo.</p>
            </div>
        </div>
    </div>




</div>