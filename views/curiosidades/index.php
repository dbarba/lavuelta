<?php

use yii\helpers\Html;
use yii\grid\GridView;

/** @var yii\web\View $this */

$this->title = 'Curiosidades';

// Registrando ScrollReveal en la vista
$this->registerJs("
    ScrollReveal({
        reset: true,
        distance: '0px',
        duration: 2500,
        delay: 400
    });

    ScrollReveal().reveal('.anim-c', { delay: 500, origin: 'bottom', interval: 200});
");
?>


<div class="site-index section-curiosidades">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Curiosidades</h1>
    </div>

    <div class="body-content n-root wrapper">

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>¿Cuantos ciclistas hay?</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $consulta5,
                            'columns' => $campos5,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>¿Cuantos puertos hay?</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $consulta6,
                            'columns' => $campos6,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Altitud media de los puertos</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $consulta7,
                            'columns' => $campos7,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="fila-2">
            <div class="anim-c">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h6>Listar los 5 ciclistas más jóvenes</h6>
                        <p>
                            <?= GridView::widget([
                                'dataProvider' => $consulta1,
                                'columns' => $campos1,
                                'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                            ]);
                            ?>
                        </p>
                    </div>
                </div>
            </div>



            <div class="anim-c">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h6>Listar los 5 ciclistas más veteranos</h6>
                        <p>
                            <?= GridView::widget([
                                'dataProvider' => $consulta2,
                                'columns' => $campos2,
                                'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                            ]);
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>¿Cuantos maillots hay?</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $consulta12,
                            'columns' => $campos12,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>¿Cuál es la pendiente media de los puertos?</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $consulta13,
                            'columns' => $campos13,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>¿Cuantas etapas hay?</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $consulta14,
                            'columns' => $campos14,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="fila-3">
            <div class="anim-c">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h6>Listar los 3 ciclistas que más etapas han ganado</h6>
                        <p>
                            <?= GridView::widget([
                                'dataProvider' => $consulta3,
                                'columns' => $campos3,
                                'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                            ]);
                            ?>
                        </p>
                    </div>
                </div>
            </div>

            <div class="anim-c">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h6>Listar los 3 ciclistas que menos etapas han ganado</h6>
                        <p>
                            <?= GridView::widget([
                                'dataProvider' => $consulta4,
                                'columns' => $campos4,
                                'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                            ]);
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>





        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Longitud media de las etapas</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $consulta8,
                            'columns' => $campos8,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>¿Cuánto dinero se dan en premios?</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $consulta15,
                            'columns' => $campos15,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Edad media de los ciclistas</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $consulta11,
                            'columns' => $campos11,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="fila-4">
            <div class="anim-c">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h6>Etapa más larga</h6>
                        <p>
                            <?= GridView::widget([
                                'dataProvider' => $consulta9,
                                'columns' => $campos9,
                                'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                            ]);
                            ?>
                        </p>
                    </div>
                </div>
            </div>



            <div class="anim-c">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h6>Etapa más corta</h6>
                        <p>
                            <?= GridView::widget([
                                'dataProvider' => $consulta10,
                                'columns' => $campos10,
                                'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                            ]);
                            ?>
                        </p>
                    </div>
                </div>
            </div>

        </div>





        <!--
        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>¿Cuántos ciclistas tiene cada equipo?</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $consulta16,
                            'columns' => $campos16,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>
 -->


        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Ciclista más joven en ganar una etapa</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $consulta17,
                            'columns' => $campos17,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Ciclista más veterano en ganar una etapa</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $consulta18,
                            'columns' => $campos18,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Ciclista más veterano en ganar un puerto</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $consulta19,
                            'columns' => $campos19,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>




        <div class="fila-5">

            <div class="anim-c">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h6>Ciclista más joven en llevar el maillot de montaña</h6>
                        <p>
                            <?= GridView::widget([
                                'dataProvider' => $consulta24,
                                'columns' => $campos24,
                                'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                            ]);
                            ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="anim-c">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h6>¿Qué ciclista ha llevado el maillot de montaña más etapas?</h6>
                        <p>
                            <?= GridView::widget([
                                'dataProvider' => $consulta22,
                                'columns' => $campos22,
                                'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                            ]);
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>


        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Ciclista más joven en ganar un puerto</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $consulta20,
                            'columns' => $campos20,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>¿Cuál es el puerto que tiene la etapa más corta?</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $consulta23,
                            'columns' => $campos23,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>¿Qué ciclista ha ganado la vuelta?</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $consulta21,
                            'columns' => $campos21,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],

                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>