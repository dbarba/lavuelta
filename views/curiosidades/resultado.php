<?php
use yii\grid\GridView;
?>

<div class="jumbotron text-center">
    <p class="lead"><?=$enunciado?></p>
</div>

<?=GridView::widget([
    'dataProvider'=> $resultados,
    'columns'=>$campos,
]);
?>
