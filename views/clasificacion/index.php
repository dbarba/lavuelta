<?php

/** @var yii\web\View $this */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\JsExpression;


$this->title = 'Clasificación';

$this->registerJsFile('@web/js/main.js', ['depends' => [\yii\web\JqueryAsset::class]]);
$this->registerJs(new JsExpression('configurarDataTable();'));


?>

<div class="section section-clasificacion n-root">
    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4"><?= Html::encode($this->title) ?></h1>
    </div>
    <?= GridView::widget([
        'dataProvider' => $consulta,
        'columns' => $campos,
        'tableOptions' => ['class' => 'table table-striped table-dark'],
        'options' => ['class' => 'grid-view'],
        'summaryOptions' => ['class' => 'summary'],
    ]); ?>
</div>