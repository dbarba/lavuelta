<?php

use app\models\Etapa;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Etapas';

// Registrando ScrollReveal en la vista
/* 
$this->registerJs("
    ScrollReveal({
        reset: true,
        distance: '0px',
        duration: 2500,
        delay: 400
    });

    ScrollReveal().reveal('.anim-c', { delay: 500, origin: 'bottom', interval: 200});
");*/
?>

<div class="site-index section-etapas">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Etapas </h1>
    </div>


    <div class="body-content n-root wrapper">
        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Etapa 1</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $etapa1,
                            'columns' => $campos1,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Etapa 2</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $etapa2,
                            'columns' => $campos2,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>


        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Etapa 3</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $etapa3,
                            'columns' => $campos3,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>


        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Etapa 4</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $etapa4,
                            'columns' => $campos4,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>



        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Etapa 5</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $etapa5,
                            'columns' => $campos5,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>


        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Etapa 6</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $etapa6,
                            'columns' => $campos6,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Etapa 7</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $etapa7,
                            'columns' => $campos7,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Etapa 8</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $etapa8,
                            'columns' => $campos8,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Etapa 9</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $etapa9,
                            'columns' => $campos9,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Etapa 10</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $etapa10,
                            'columns' => $campos10,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Etapa 11</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $etapa11,
                            'columns' => $campos11,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Etapa 12</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $etapa12,
                            'columns' => $campos12,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Etapa 13</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $etapa13,
                            'columns' => $campos13,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Etapa 14</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $etapa14,
                            'columns' => $campos14,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Etapa 15</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $etapa15,
                            'columns' => $campos15,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Etapa 16</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $etapa16,
                            'columns' => $campos16,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Etapa 17</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $etapa17,
                            'columns' => $campos17,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Etapa 18</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $etapa18,
                            'columns' => $campos18,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Etapa 19</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $etapa19,
                            'columns' => $campos19,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Etapa 20</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $etapa20,
                            'columns' => $campos20,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="anim-c">
            <div class="card alturaminima">
                <div class="card-body tarjeta">
                    <h6>Etapa 21</h6>
                    <p>
                        <?= GridView::widget([
                            'dataProvider' => $etapa21,
                            'columns' => $campos21,
                            'tableOptions' => ['class' => 'table table-striped table-dark table-bordered'],
                        ]);
                        ?>
                    </p>
                </div>
            </div>
        </div>

    </div>
</div>