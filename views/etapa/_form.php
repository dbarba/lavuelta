<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Etapa $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="etapa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'numetapa')->textInput() ?>

    <?= $form->field($model, 'kms')->textInput() ?>

    <?= $form->field($model, 'salida')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'llegada')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dorsal')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
