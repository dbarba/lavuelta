<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;



AppAsset::register($this);

$this->registerCsrfMetaTags();
$this->registerMetaTag(['charset' => Yii::$app->charset], 'charset');
$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, shrink-to-fit=no']);
$this->registerMetaTag(['name' => 'description', 'content' => $this->params['meta_description'] ?? '']);
$this->registerMetaTag(['name' => 'keywords', 'content' => $this->params['meta_keywords'] ?? '']);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => Yii::getAlias('@web/img/logo.ico')]);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<?= Html::csrfMetaTags() ?>

<head>
    <title>La Vuelta Al Pasado</title> <!-- Título predeterminado -->
    <?php $this->head() ?>
</head>

<body class="d-flex flex-column h-100">
    <?php $this->beginBody() ?>

    <header id="header">
        <?php
        NavBar::begin([
            'brandLabel' => '<img src="' . Yii::getAlias('@web/img/logo.png') . '" alt="Logo" class="logo">',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => ['class' => 'navbar-expand-md fixed-top']
        ]);
        echo '<div class="nav-container w-100">';
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => [
                '<div class="menu-items">
                <div class="text-container">
                    <div class="navbar-title">La Vuelta</div>
                    <div class="navbar-subtitle">Al Pasado</div>
                </div>
                <div class="menu-links">
                    <span>' . Html::a('Inicio', ['/site/index'], ['class' => 'nav-link']) . '</span>
                    <span>' . Html::a('Equipos', ['/equipo/index'], ['class' => 'nav-link']) . '</span>
                    <span>' . Html::a('Etapas', ['/etapa/index'], ['class' => 'nav-link']) . '</span>
                    <span>' . Html::a('Clasificacion', ['/clasificacion/index'], ['class' => 'nav-link']) . '</span>
                    <span>' . Html::a('Puertos', ['/puerto/index'], ['class' => 'nav-link']) . '</span>
                    <span>' . (Yii::$app->user->isGuest ?
                    Html::a('Curiosidades', ['/curiosidades/index'], ['class' => 'nav-link']) :
                    Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        'Logout (' . Yii::$app->user->identity->username . ')',
                        ['class' => 'nav-link btn btn-link logout']
                    )
                    . Html::endForm()) . '</span>
                </div>
            </div>',
                '<div class="social-links">
                <span>' . Html::a('<i class="fab fa-gitlab"></i>', 'https://gitlab.com/Proyecto4AdaLovelace/ada-lovelace-ciclistas', ['class' => 'nav-link']) . '</span>
                <span>' . Html::a('<i class="fa-brands fa-linkedin-in"></i>', '#', ['class' => 'nav-link']) . '</span>
            </div>',
            ]
        ]);
        echo '</div>';
        NavBar::end();
        ?>
    </header>






    <main role="main" class="flex-shrink-0">
        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </main>

    <footer id="footer" class="mt-auto py-3">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-6 text-center text-md-start text-center text-secondary footer-info">&copy; Ada Lovelace <?= date('Y') ?></div>
            </div>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>