// VARIABLES
var navbar = document.querySelector("#header .navbar");

// EVENTOS
window.addEventListener("scroll", () => {
  navbarSCroll();
});

// MÉTODOS - FUNCIONES

// navbar-scroll.js
function navbarSCroll() {
  if (window.scrollY > 50) {
    navbar.classList.add("scrolled");
  } else {
    navbar.classList.remove("scrolled");
  }
}
