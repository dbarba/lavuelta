<?php

namespace app\controllers;

//use app\controllers\Controller;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ciclista;
use app\models\Etapa;
use app\models\Lleva;
use app\models\Maillot;
use app\models\Puerto;
use app\models\Equipo;
use yii\web\View;
use yii\data\SqlDataProvider;
use yii\web\Controller;


/* 
 
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
*/

class ClasificacionController extends Controller
{
    public function actionIndex()
    {
        // Resto de la acción
        return $this->render('index', [
            'consulta' => new SqlDataProvider([
                'sql' => 'SELECT DISTINCT e.numetapa, e.salida, e.llegada, l.dorsal, c.nombre, m.tipo
                      FROM maillot m INNER JOIN lleva l INNER JOIN ciclista c INNER JOIN etapa e  ON m.código = l.código 
                      AND l.numetapa = e.numetapa  AND l.dorsal = c.dorsal ORDER BY e.numetapa',
            ]),

            'campos' => ['numetapa', 'salida', 'llegada', 'dorsal', 'nombre', 'tipo'],

        ]);
    }
}
