<?php

namespace app\controllers;

use app\models\Etapa;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use yii\data\SqlDataProvider;
/**
 * EtapaController implements the CRUD actions for Etapa model.
 */
class EtapaController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Etapa models.
     *
     * @return string
     */
    public function actionIndex()
    {
         return $this->render('index', [
            'etapa1' => new SqlDataProvider ([
                "sql" => "select salida, llegada, kms,dorsal, nombre from etapa  INNER JOIN ciclista  USING(dorsal) where numetapa = 1 AND dorsal = dorsal",
            ]),
            "campos1" => ["salida" , "llegada", "kms", "dorsal", "nombre"],
             
              'etapa2' => new SqlDataProvider ([
                "sql" => "select salida, llegada, kms,dorsal, nombre from etapa  INNER JOIN ciclista  USING(dorsal) where numetapa = 2 AND dorsal = dorsal",
            ]),
            "campos2" => ["salida" , "llegada", "kms", "dorsal", "nombre"],
             
             
             'etapa3' => new SqlDataProvider ([
                "sql" => "select salida, llegada, kms,dorsal, nombre from etapa  INNER JOIN ciclista  USING(dorsal) where numetapa = 3 AND dorsal = dorsal",
            ]),
            "campos3" => ["salida" , "llegada", "kms", "dorsal", "nombre"],
             
             'etapa4' => new SqlDataProvider ([
                "sql" => "select salida, llegada, kms,dorsal, nombre from etapa  INNER JOIN ciclista  USING(dorsal) where numetapa = 4 AND dorsal = dorsal",
            ]),
            "campos4" => ["salida" , "llegada", "kms", "dorsal", "nombre"],
             
             'etapa5' => new SqlDataProvider ([
                "sql" => "select salida, llegada, kms,dorsal, nombre from etapa  INNER JOIN ciclista  USING(dorsal) where numetapa = 5 AND dorsal = dorsal",
            ]),
            "campos5" => ["salida" , "llegada", "kms", "dorsal", "nombre"],
             
             'etapa6' => new SqlDataProvider ([
                "sql" => "select salida, llegada, kms,dorsal, nombre from etapa  INNER JOIN ciclista  USING(dorsal) where numetapa = 6 AND dorsal = dorsal",
            ]),
            "campos6" => ["salida" , "llegada", "kms", "dorsal", "nombre"],
             
             'etapa7' => new SqlDataProvider ([
                "sql" => "select salida, llegada, kms,dorsal, nombre from etapa  INNER JOIN ciclista  USING(dorsal) where numetapa = 7 AND dorsal = dorsal",
            ]),
            "campos7" => ["salida" , "llegada", "kms", "dorsal", "nombre"],
             
             'etapa8' => new SqlDataProvider ([
                "sql" => "select salida, llegada, kms,dorsal, nombre from etapa  INNER JOIN ciclista  USING(dorsal) where numetapa = 8 AND dorsal = dorsal",
            ]),
            "campos8" => ["salida" , "llegada", "kms", "dorsal", "nombre"],
             
             'etapa9' => new SqlDataProvider ([
                "sql" => "select salida, llegada, kms,dorsal, nombre from etapa  INNER JOIN ciclista  USING(dorsal) where numetapa = 9 AND dorsal = dorsal",
            ]),
            "campos9" => ["salida" , "llegada", "kms", "dorsal", "nombre"],
             
             'etapa10' => new SqlDataProvider ([
                "sql" => "select salida, llegada, kms,dorsal, nombre from etapa  INNER JOIN ciclista  USING(dorsal) where numetapa = 10 AND dorsal = dorsal",
            ]),
            "campos10" => ["salida" , "llegada", "kms", "dorsal", "nombre"],
             
             'etapa11' => new SqlDataProvider ([
                "sql" => "select salida, llegada, kms,dorsal, nombre from etapa  INNER JOIN ciclista  USING(dorsal) where numetapa = 11 AND dorsal = dorsal",
            ]),
            "campos11" => ["salida" , "llegada", "kms", "dorsal", "nombre"],
             
             'etapa12' => new SqlDataProvider ([
                "sql" => "select salida, llegada, kms,dorsal, nombre from etapa  INNER JOIN ciclista  USING(dorsal) where numetapa = 12 AND dorsal = dorsal",
            ]),
            "campos12" => ["salida" , "llegada", "kms", "dorsal", "nombre"],
             
             'etapa13' => new SqlDataProvider ([
                "sql" => "select salida, llegada, kms,dorsal, nombre from etapa  INNER JOIN ciclista  USING(dorsal) where numetapa = 13 AND dorsal = dorsal",
            ]),
            "campos13" => ["salida" , "llegada", "kms", "dorsal", "nombre"],
             
             'etapa14' => new SqlDataProvider ([
                "sql" => "select salida, llegada, kms,dorsal, nombre from etapa  INNER JOIN ciclista  USING(dorsal) where numetapa = 14 AND dorsal = dorsal",
            ]),
            "campos14" => ["salida" , "llegada", "kms", "dorsal", "nombre"],
             
             'etapa15' => new SqlDataProvider ([
                "sql" => "select salida, llegada, kms,dorsal, nombre from etapa  INNER JOIN ciclista  USING(dorsal) where numetapa = 15 AND dorsal = dorsal",
            ]),
            "campos15" => ["salida" , "llegada", "kms", "dorsal", "nombre"],
             
             'etapa16' => new SqlDataProvider ([
                "sql" => "select salida, llegada, kms,dorsal, nombre from etapa  INNER JOIN ciclista  USING(dorsal) where numetapa = 16 AND dorsal = dorsal",
            ]),
            "campos16" => ["salida" , "llegada", "kms", "dorsal", "nombre"],
             
             'etapa17' => new SqlDataProvider ([
                "sql" => "select salida, llegada, kms,dorsal, nombre from etapa  INNER JOIN ciclista  USING(dorsal) where numetapa = 17 AND dorsal = dorsal",
            ]),
            "campos17" => ["salida" , "llegada", "kms", "dorsal", "nombre"],
             
             'etapa18' => new SqlDataProvider ([
                "sql" => "select salida, llegada, kms,dorsal, nombre from etapa  INNER JOIN ciclista  USING(dorsal) where numetapa = 18 AND dorsal = dorsal",
            ]),
            "campos18" => ["salida" , "llegada", "kms", "dorsal", "nombre"],
             
             'etapa19' => new SqlDataProvider ([
                "sql" => "select salida, llegada, kms,dorsal, nombre from etapa  INNER JOIN ciclista  USING(dorsal) where numetapa = 19 AND dorsal = dorsal",
            ]),
            "campos19" => ["salida" , "llegada", "kms", "dorsal", "nombre"],
             
             'etapa20' => new SqlDataProvider ([
                "sql" => "select salida, llegada, kms,dorsal, nombre from etapa  INNER JOIN ciclista  USING(dorsal) where numetapa = 20 AND dorsal = dorsal",
            ]),
            "campos20" => ["salida" , "llegada", "kms", "dorsal", "nombre"],
             
             'etapa21' => new SqlDataProvider ([
                "sql" => "select salida, llegada, kms,dorsal, nombre from etapa  INNER JOIN ciclista  USING(dorsal) where numetapa = 21 AND dorsal = dorsal",
            ]),
            "campos21" => ["salida" , "llegada", "kms", "dorsal", "nombre"],
        ]);
    }

    /**
     * Displays a single Etapa model.
     * @param int $numetapa Numetapa
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($numetapa)
    {
        return $this->render('view', [
            'model' => $this->findModel($numetapa),
        ]);
    }

    /**
     * Creates a new Etapa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Etapa();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'numetapa' => $model->numetapa]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Etapa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $numetapa Numetapa
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($numetapa)
    {
        $model = $this->findModel($numetapa);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'numetapa' => $model->numetapa]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Etapa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $numetapa Numetapa
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($numetapa)
    {
        $this->findModel($numetapa)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Etapa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $numetapa Numetapa
     * @return Etapa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($numetapa)
    {
        if (($model = Etapa::findOne(['numetapa' => $numetapa])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
