<?php

namespace app\controllers;

use yii\web\Controller;

use yii\data\SqlDataProvider;

class CuriosidadesController extends Controller
{
    public function actionIndex()
    {
        // Acción para la página de inicio
        return $this->render('index', [
            "consulta1"=>new SqlDataProvider ([
                "sql"=>"SELECT DISTINCT nombre, edad FROM ciclista ORDER BY edad LIMIT 5",
                "pagination" => false,
                ]),
            "campos1"=>['nombre', 'edad'],
            
            "consulta2"=>new SqlDataProvider ([
                "sql"=>"SELECT DISTINCT nombre, edad FROM ciclista ORDER BY edad desc LIMIT 5",
                "pagination" => false,
                ]),
            "campos2"=>['nombre', 'edad'],
            
            "consulta3"=>new SqlDataProvider ([
                "sql"=>"SELECT nombre, total 
                            FROM ciclista INNER JOIN 
                            (select dorsal, count(*) as total from etapa group by dorsal order by 2 desc limit 3)a3 
                            USING(dorsal)",
                
                ]),
            "campos3"=>['nombre', 'total'],
            
            "consulta4"=>new SqlDataProvider ([
                "sql"=>"SELECT nombre, total 
                            FROM ciclista INNER JOIN 
                            (select dorsal, count(*) as total from etapa group by dorsal order by 2 limit 3)a3 
                            USING(dorsal)",
                
                ]),
            "campos4"=>['nombre', 'total'],
            
            "consulta5"=>new SqlDataProvider ([
                "sql"=>"SELECT count(*) AS total FROM ciclista",
                ]),
            "campos5"=>['total'],
            
            "consulta6"=>new SqlDataProvider ([
                "sql"=>"SELECT count(*) AS total FROM puerto",
                ]),
            "campos6"=>['total'],
            
            "consulta7"=>new SqlDataProvider ([
                "sql"=>"SELECT avg(altura) AS media FROM puerto",
                ]),
            "campos7"=>['media'],
            
            "consulta8"=>new SqlDataProvider ([
                "sql"=>"SELECT avg(kms) AS media FROM etapa",
                ]),
            "campos8"=>['media'],
            
            "consulta9"=>new SqlDataProvider ([
                "sql"=>"SELECT DISTINCT  numetapa, kms, salida, llegada FROM etapa WHERE kms =(SELECT MAX(kms) AS media FROM etapa)",
                ]),
            "campos9"=>['numetapa', 'kms', 'salida', 'llegada'],
            
            "consulta10"=>new SqlDataProvider ([
                "sql"=>"SELECT DISTINCT  numetapa, kms, salida, llegada FROM etapa WHERE kms =(SELECT MIN(kms) AS media FROM etapa)",
                ]),
            "campos10"=>['numetapa', 'kms', 'salida', 'llegada'],
            
            "consulta11"=>new SqlDataProvider ([
                "sql"=>"SELECT AVG(edad) AS media FROM ciclista",
                ]),
            "campos11"=>['media'],
            
            "consulta12"=>new SqlDataProvider ([
                "sql"=>"SELECT count(*) AS total FROM maillot",
                ]),
            "campos12"=>['total'],
            
            "consulta13"=>new SqlDataProvider ([
                "sql"=>"SELECT avg(pendiente) AS media FROM puerto",
                ]),
            "campos13"=>['media'],
            
            "consulta14"=>new SqlDataProvider ([
                "sql"=>"SELECT count(*) AS total FROM etapa",
                ]),
            "campos14"=>['total'],
            
            "consulta15"=>new SqlDataProvider ([
                "sql"=>"SELECT sum(premio) AS total FROM maillot",
                ]),
            "campos15"=>['total'],
            
            "consulta16"=>new SqlDataProvider ([
                "sql"=>"SELECT nomequipo, count(*) AS total from ciclista group by nomequipo",
                ]),
            "campos16"=>['nomequipo', 'total'],
            
            "consulta17"=>new SqlDataProvider ([
                "sql"=>"SELECT c.nombre, MIN(c.edad) AS edad
                    FROM ciclista c INNER JOIN etapa e
                    USING(dorsal)
                    GROUP BY c.nombre
                    ORDER BY edad ASC
                    LIMIT 1
                    ",
                    "pagination" => false,
                ]),
            "campos17"=>['nombre', 'edad'],
            
            "consulta18"=>new SqlDataProvider ([
                "sql"=>"SELECT c.nombre, MIN(c.edad) AS edad
                    FROM ciclista c INNER JOIN etapa e
                    USING(dorsal)
                    GROUP BY c.nombre
                    ORDER BY edad DESC
                    LIMIT 1
                    ",
                    "pagination" => false,
                ]),
            "campos18"=>['nombre', 'edad'],
            
            "consulta19"=>new SqlDataProvider ([
                "sql"=>"SELECT c.nombre, MIN(c.edad) AS edad
                    FROM ciclista c INNER JOIN puerto p
                    USING(dorsal)
                    GROUP BY c.nombre
                    ORDER BY edad DESC
                    LIMIT 1
                    ",
                    "pagination" => false,
                ]),
            "campos19"=>['nombre', 'edad'],
            
            "consulta20"=>new SqlDataProvider ([
                "sql"=>"SELECT c.nombre, MIN(c.edad) AS edad
                    FROM ciclista c INNER JOIN puerto p
                    USING(dorsal)
                    GROUP BY c.nombre
                    ORDER BY edad ASC
                    LIMIT 1
                    ",
                    "pagination" => false,
                ]),
            "campos20"=>['nombre', 'edad'],
            
            "consulta21"=>new SqlDataProvider ([
                "sql"=>'SELECT dorsal, nombre 
                    FROM ciclista 
                    WHERE dorsal = (
                        SELECT DISTINCT dorsal
                            FROM lleva 
                            WHERE código = (
                                SELECT DISTINCT código
                                    FROM maillot 
                                    WHERE tipo = "General"
                            ) AND numetapa = (
                                SELECT COUNT(*)
                                    FROM etapa))',
                ]),
            "campos21"=>['dorsal', 'nombre'],
            
            "consulta22"=>new SqlDataProvider ([
                "sql"=>'SELECT DISTINCT a3.dorsal, c.nombre, total FROM ciclista c INNER JOIN ( 
                    SELECT * FROM (
                        SELECT dorsal, COUNT(*) AS total FROM lleva WHERE código = (
                            SELECT DISTINCT código FROM maillot WHERE tipo = "Montaña")
                        GROUP BY dorsal)a1
                    WHERE a1.total = (
                        SELECT MAX(total) FROM (
                            SELECT dorsal, COUNT(*) AS total FROM lleva WHERE código = (
                                SELECT DISTINCT código FROM maillot WHERE tipo = "Montaña")
                            GROUP BY dorsal)a1))a3
                ON c.dorsal = a3.dorsal',
                ]),
            "campos22"=>['dorsal', 'nombre', 'total'],
            
            "consulta23"=>new SqlDataProvider ([
                "sql"=>'SELECT DISTINCT nompuerto, numetapa FROM puerto WHERE numetapa = (
                    SELECT DISTINCT numetapa FROM etapa WHERE kms = (
                        SELECT MIN(kms) FROM etapa WHERE numetapa IN (
                            SELECT DISTINCT numetapa FROM puerto WHERE numetapa IN (
                                SELECT DISTINCT  numetapa FROM puerto))))',
                ]),
            "campos23"=>['nompuerto', 'numetapa'],
            
            "consulta24"=>new SqlDataProvider ([
                "sql"=>'SELECT c.dorsal, c.nombre, MIN(c.edad) AS edad FROM ciclista c INNER JOIN lleva l USING(dorsal) WHERE l.código = (
                    SELECT DISTINCT código FROM maillot WHERE tipo = "Montaña")
                    GROUP BY c.nombre ORDER BY edad ASC LIMIT 1',
                "pagination" => false,
                ]),
            "campos24"=>['dorsal', 'nombre', 'edad'],
        ]);
    }

    
    
    
    /*Ciclista más joven en llevar el maillot de montaña*/
    public function actionConsulta24(){
        $dataProvider = new SqlDataProvider ([
        "sql"=>'SELECT c.dorsal, c.nombre, MIN(c.edad) AS edad FROM ciclista c INNER JOIN lleva l USING(dorsal) WHERE l.código = (
                    SELECT DISTINCT código FROM maillot WHERE tipo = "Montaña")
                    GROUP BY c.nombre ORDER BY edad ASC LIMIT 1',
        "pagination" => false,
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal', 'nombre', 'edad'],
            "enunciado"=>"Ciclista más joven en llevar el maillot de montaña",
        ]);   
    }

}
