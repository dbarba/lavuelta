<?php

namespace app\controllers;

use app\models\Puerto;
use app\models\Ciclista;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;

/**
 * PuertoController implements the CRUD actions for Puerto model.
 */
class PuertoController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Puerto models.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'puerto1' => new SqlDataProvider ([
                "sql" => 'SELECT DISTINCT altura, categoria, pendiente, numetapa, dorsal, nombre FROM puerto INNER JOIN ciclista USING(dorsal) WHERE nompuerto = "Coll de Espina" AND dorsal = dorsal',
            ]),
            "campos1" => ["altura" , "categoria", "pendiente", "numetapa", "dorsal", "nombre"],
            
            'puerto2' => new SqlDataProvider ([
                "sql" => 'SELECT DISTINCT altura, categoria, pendiente, numetapa, dorsal, nombre FROM puerto INNER JOIN ciclista USING(dorsal) WHERE nompuerto = "Coll de Montaup" AND dorsal = dorsal',
            ]),
            "campos2" => ["altura" , "categoria", "pendiente", "numetapa", "dorsal", "nombre"],
            
            'puerto3' => new SqlDataProvider ([
                "sql" => 'SELECT DISTINCT altura, categoria, pendiente, numetapa, dorsal, nombre FROM puerto INNER JOIN ciclista USING(dorsal) WHERE nompuerto = "Coll de sa Batalla" AND dorsal = dorsal',
            ]),
            "campos3" => ["altura" , "categoria", "pendiente", "numetapa", "dorsal", "nombre"],
            
            'puerto4' => new SqlDataProvider ([
                "sql" => 'SELECT DISTINCT altura, categoria, pendiente, numetapa, dorsal, nombre FROM puerto INNER JOIN ciclista USING(dorsal) WHERE nompuerto = "Estación de Esquí de Cerler" AND dorsal = dorsal',
            ]),
            "campos4" => ["altura" , "categoria", "pendiente", "numetapa", "dorsal", "nombre"],
            
            'puerto5' => new SqlDataProvider ([
                "sql" => 'SELECT DISTINCT altura, categoria, pendiente, numetapa, dorsal, nombre FROM puerto INNER JOIN ciclista USING(dorsal) WHERE nompuerto = "Laguna Negra de Niela" AND dorsal = dorsal',
            ]),
            "campos5" => ["altura" , "categoria", "pendiente", "numetapa", "dorsal", "nombre"],
            
            'puerto6' => new SqlDataProvider ([
                "sql" => 'SELECT DISTINCT altura, categoria, pendiente, numetapa, dorsal, nombre FROM puerto INNER JOIN ciclista USING(dorsal) WHERE nompuerto = "Puerto de Albaida" AND dorsal = dorsal',
            ]),
            "campos6" => ["altura" , "categoria", "pendiente", "numetapa", "dorsal", "nombre"],
            
            'puerto7' => new SqlDataProvider ([
                "sql" => 'SELECT DISTINCT altura, categoria, pendiente, numetapa, dorsal, nombre FROM puerto INNER JOIN ciclista USING(dorsal) WHERE nompuerto = "Puerto de Cotos" AND dorsal = dorsal',
            ]),
            "campos7" => ["altura" , "categoria", "pendiente", "numetapa", "dorsal", "nombre"],
            
            'puerto8' => new SqlDataProvider ([
                "sql" => 'SELECT DISTINCT altura, categoria, pendiente, numetapa, dorsal, nombre FROM puerto INNER JOIN ciclista USING(dorsal) WHERE nompuerto = "Puerto de la Carrasqueta" AND dorsal = dorsal',
            ]),
            "campos8" => ["altura" , "categoria", "pendiente", "numetapa", "dorsal", "nombre"],
            
            'puerto9' => new SqlDataProvider ([
                "sql" => 'SELECT DISTINCT altura, categoria, pendiente, numetapa, dorsal, nombre FROM puerto INNER JOIN ciclista USING(dorsal) WHERE nompuerto = "Puerto de la Cruz de Perves" AND dorsal = dorsal',
            ]),
            "campos9" => ["altura" , "categoria", "pendiente", "numetapa", "dorsal", "nombre"],
            
            'puerto10' => new SqlDataProvider ([
                "sql" => 'SELECT DISTINCT altura, categoria, pendiente, numetapa, dorsal, nombre FROM puerto INNER JOIN ciclista USING(dorsal) WHERE nompuerto = "Puerto de la Cruz verde" AND dorsal = dorsal',
            ]),
            "campos10" => ["altura" , "categoria", "pendiente", "numetapa", "dorsal", "nombre"],
            
            'puerto11' => new SqlDataProvider ([
                "sql" => 'SELECT DISTINCT altura, categoria, pendiente, numetapa, dorsal, nombre FROM puerto INNER JOIN ciclista USING(dorsal) WHERE nompuerto = "Puerto de la Morcueraa" AND dorsal = dorsal',
            ]),
            "campos11" => ["altura" , "categoria", "pendiente", "numetapa", "dorsal", "nombre"],
            
            'puerto12' => new SqlDataProvider ([
                "sql" => 'SELECT DISTINCT altura, categoria, pendiente, numetapa, dorsal, nombre FROM puerto INNER JOIN ciclista USING(dorsal) WHERE nompuerto = "Puerto de Leon" AND dorsal = dorsal',
            ]),
            "campos12" => ["altura" , "categoria", "pendiente", "numetapa", "dorsal", "nombre"],
            
            'puerto13' => new SqlDataProvider ([
                "sql" => 'SELECT DISTINCT altura, categoria, pendiente, numetapa, dorsal, nombre FROM puerto INNER JOIN ciclista USING(dorsal) WHERE nompuerto = "Puerto de Navacerrada" AND dorsal = dorsal',
            ]),
            "campos13" => ["altura" , "categoria", "pendiente", "numetapa", "dorsal", "nombre"],
            
            'puerto14' => new SqlDataProvider ([
                "sql" => 'SELECT DISTINCT altura, categoria, pendiente, numetapa, dorsal, nombre FROM puerto INNER JOIN ciclista USING(dorsal) WHERE nompuerto = "Puerto de Pedro Bernardo" AND dorsal = dorsal',
            ]),
            "campos14" => ["altura" , "categoria", "pendiente", "numetapa", "dorsal", "nombre"],
            
            'puerto15' => new SqlDataProvider ([
                "sql" => 'SELECT DISTINCT altura, categoria, pendiente, numetapa, dorsal, nombre FROM puerto INNER JOIN ciclista USING(dorsal) WHERE nompuerto = "Puerto de Tibi" AND dorsal = dorsal',
            ]),
            "campos15" => ["altura" , "categoria", "pendiente", "numetapa", "dorsal", "nombre"],
            
            'puerto16' => new SqlDataProvider ([
                "sql" => 'SELECT DISTINCT altura, categoria, pendiente, numetapa, dorsal, nombre FROM puerto INNER JOIN ciclista USING(dorsal) WHERE nompuerto = "Sierra Nevada" AND dorsal = dorsal',
            ]),
            "campos16" => ["altura" , "categoria", "pendiente", "numetapa", "dorsal", "nombre"],
        ]);
    }

    /**
     * Displays a single Puerto model.
     * @param string $nompuerto Nompuerto
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($nompuerto)
    {
        return $this->render('view', [
            'model' => $this->findModel($nompuerto),
        ]);
    }

    /**
     * Creates a new Puerto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Puerto();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'nompuerto' => $model->nompuerto]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Puerto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $nompuerto Nompuerto
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($nompuerto)
    {
        $model = $this->findModel($nompuerto);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'nompuerto' => $model->nompuerto]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Puerto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $nompuerto Nompuerto
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($nompuerto)
    {
        $this->findModel($nompuerto)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Puerto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $nompuerto Nompuerto
     * @return Puerto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($nompuerto)
    {
        if (($model = Puerto::findOne(['nompuerto' => $nompuerto])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
