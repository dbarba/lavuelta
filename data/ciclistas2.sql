﻿/* creacion de la base de datos */
DROP DATABASE IF EXISTS ciclistas2;
CREATE DATABASE IF NOT EXISTS ciclistas2 
  CHARACTER SET utf8 
  COLLATE utf8_spanish_ci;

/* codificacion en transmision castellano */
SET NAMES 'utf8';

/* SELECCIONAMOS LA BASE DE DATOS */
USE ciclistas2;

/* CREAR TABLA EQUIPO */
CREATE TABLE IF NOT EXISTS equipo (
  -- CREAMOS LOS CAMPOS
  nomequipo varchar(25) NOT NULL,
  director varchar(30) DEFAULT NULL,
  -- COLOCAMOS RESTRICCIONES, CLAVES Y CHECKS
  PRIMARY KEY (nomequipo)
)
  -- OPCIONES DE LA TABLA
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_spanish_ci; 

/* crear tabla maillot */
CREATE TABLE IF NOT EXISTS maillot (
-- CREAR CAMPOS
  código varchar(3) NOT NULL,
  tipo varchar(30) NOT NULL,
  color varchar(20) NOT NULL,
  premio int(10) NOT NULL,
  -- RESTRICCIONES
  PRIMARY KEY (código)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

/* CREAR TABLA CICLISTA */

CREATE TABLE IF NOT EXISTS ciclista (
  -- CREAMOS LOS CAMPOS
  dorsal smallint(5) NOT NULL,
  nombre varchar(30) NOT NULL,
  edad smallint(5) DEFAULT NULL,
  nomequipo varchar(25) NOT NULL,
  -- RESTRICCIONES
  PRIMARY KEY (dorsal), -- CLAVE PRINCIPAL
  INDEX ciclistanomequipo (nomequipo), -- INDEXADO CON DUPLICADOS
  CONSTRAINT fk_ciclista_equipo 
      FOREIGN KEY (nomequipo) REFERENCES equipo(nomequipo) 
      ON DELETE CASCADE ON UPDATE CASCADE -- CLAVE AJENA
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_spanish_ci; 

/* creamos la tabla etapa */
CREATE TABLE IF NOT EXISTS etapa (
  -- campos
  numetapa smallint(5) NOT NULL,
  kms smallint(5) NOT NULL,
  salida varchar(35) NOT NULL,
  llegada varchar(35) NOT NULL,
  dorsal smallint(5) DEFAULT NULL,
  -- restricciones
  PRIMARY KEY (numetapa),
  CONSTRAINT fk_etapa_ciclista FOREIGN KEY (dorsal) REFERENCES 
    ciclista(dorsal) ON DELETE CASCADE ON UPDATE CASCADE   
)
  -- opciones de tabla
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

/* crear tabla lleva */
CREATE TABLE IF NOT EXISTS lleva (
  -- campos
  dorsal smallint(5) NOT NULL,
  numetapa smallint(5) NOT NULL,
  código varchar(3) NOT NULL,
  -- restricciones
  PRIMARY KEY (numetapa, código),
  INDEX FK_lleva_etapa_numetapa (numetapa),
  CONSTRAINT FK_lleva_etapa_numetapa FOREIGN KEY (numetapa)
    REFERENCES ciclistas.etapa (numetapa) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_lleva_maillot_código FOREIGN KEY (código)
    REFERENCES ciclistas.maillot (código) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_lleva_ciclista FOREIGN KEY (dorsal)
    REFERENCES ciclista (dorsal) ON DELETE CASCADE ON UPDATE CASCADE
)
  -- opciones
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_spanish_ci;



/* CRER TABLA PUERTO */
CREATE TABLE IF NOT EXISTS puerto (
  nompuerto varchar(35) NOT NULL,
  altura smallint(5) NOT NULL,
  categoria varchar(1) NOT NULL,
  pendiente double(15, 5) DEFAULT NULL,
  numetapa smallint(5) NOT NULL,
  dorsal smallint(5) DEFAULT NULL,
  PRIMARY KEY (nompuerto),
  CONSTRAINT fk_puerto_etapa FOREIGN KEY (numetapa) 
    REFERENCES etapa(numetapa) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_puerto_ciclista FOREIGN KEY (dorsal) 
    REFERENCES ciclista (dorsal) ON DELETE CASCADE ON UPDATE CASCADE     
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

-- Disable foreign key checks
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;

INSERT INTO `ciclistas2`.`ciclista`(`dorsal`, `nombre`, `edad`, `nomequipo`)
VALUES (1, 'ZULLE Alex', 28, 'Lotus-Festina'),
  (2, 'BROCHARD Laurent', 35, 'Lotus-Festina'),
  (3, 'DUFAUX Laurent', 27, 'Lotus-Festina'),
  (4, 'GARCIA CASAS Félix Manuel', 30, 'Lotus-Festina'),
  (5, 'HERVE Pascal', 32, 'Lotus-Festina'),
  (6, 'ROUS Didier', 33, 'Lotus-Festina'),
  (7, 'URIARTE José Ramón', 30, 'Lotus-Festina'),
  (8, 'VIRENQUE Richard', 33, 'Lotus Festina'),
  (9, 'WUST Marcel', 34, 'Lotus-Festina'),

  (11, 'CASTELBLANCO ROMERO José Joaquim', 31, 'Avianca-Telecom'),
  (12, 'AGUIRRE CABRERA Julio César', 31, 'Avianca-Telecom'),
  (13, 'HERNANDEZ MONTOYA Jairo de Jesús', 28, 'Avianca-Telecom'),
  (14, 'LEON MANE Francisco', 33, 'Avianca-Telecom'),
  (15, 'PASCUAL LLORENTE Javier', 31, 'Avianca-Telecom'),
  (16, 'MESA SAAVEDRA Luis Ricardo', 29, 'Avianca-Telecom'),
  (17, 'MUÑOZ FERNANDEZ Federico', 37, 'Avianca-Telecom'),
  (18, 'OSORIO ALONSO Carlos Andrés', 37, 'Avianca-Telecom'),
  (19, 'PEÑA GRISALES Victor Hugo', 34, 'Avianca-Telecom'),
  
  (21, 'OLANO MANZANO Abraham', 31, 'Banesto'),
  (22, 'BELTRAN MARTINEZ Manuel', 32, 'Banesto'),
  (23, 'DE LAS CUEVAS Armand', 27, 'Banesto'),
  (24, 'FERNANDEZ GINES Manuel', 29, 'Banesto'),
  (25, 'GARCIA ACOSTA José Vicente', 32, 'Banesto'),
  (26, 'GARMENDIA ARBILLA Aitor', 27, 'Banesto'),
  (27, 'JIMENEZ SASTRE José Maria', 28, 'Banesto'),
  (28, 'ODRIOZOLA MUGARZA Jon', 33, 'Banesto'),
  (29, 'OSA EIZAGUIRRE Aitor', 28, 'Banesto'),
  
  (31, 'CONTRINI Daniele', 30, 'Brescialat-Liquigas'),
  (32, 'FRATTINI Cristiano', 28, 'Brescialat-Liquigas'),
  (33, 'IVANOV Ruslan', 29, 'Brescialat-Liquigas'),
  (34, 'MILESI Marco', 30, 'Brescialat-Liquigas'),
  (35, 'PICCOLI Mariano', 28, 'Brescialat-Liquigas'),
  (36, 'RAIMONDI Giancarlo', 32, 'Brescialat-Liquigas'),
  (37, 'SERPELLINI Marco', 29, 'Brescialat-Liquigas'),
  (38, 'SGAMBELLURI Roberto', 25, 'Brescialat-Liquigas'),
  (39, 'MORENI Cristian', 30, 'Brescialat-Liquigas'),
  
  (41, 'GONTCHAR Sergei', 25, 'Cantina Tollo'),
  (42, 'FRIZZO Maurizio', 29, 'Cantina Tollo'),
  (43, 'GENTILI Massimiliano', 28, 'Cantina Tollo'),
  (44, 'GIUNTI Massimo', 27, 'Cantina Tollo'),
  (45, 'HVASTIJA Martin', 26, 'Cantina Tollo'),
  (46, 'RITTSEL Martin', 24, 'Cantina Tollo'),
  (47, 'SIMONI Gilberto', 32, 'Cantina Tollo'),
  (48, 'STRAZZER Massimo', 29, 'Cantina Tollo'),
  (49, 'TRENTI Guido', 23, 'Cantina Tollo'),

  (51, 'AGNOLUTTO Christophe', 30, 'Casino'),
  (52, 'AUS Lauri', 26, 'Casino'),
  (53, 'BESSY Frédéric', 28, 'Casino'),
  (54, 'CALI Vincent', 30, 'Casino'),
  (55, 'CHANTEUR Pascal', 28, 'Casino'),
  (56, 'GOUGOT Fabrice', 29, 'Casino'),
  (57, 'KASPUTIS Arturas', 28, 'Casino'),
  (58, 'KIRSIPUU Jaan', 34, 'Casino'),
  (59, 'MASSI Rodolfo', 28, 'Casino'),
  
  (61, 'BERTOLINI Alessandro', 26, 'Cofidis'),
  (62, 'CAPELLE Christophe', 30, 'Cofidis'),
  (63, 'FONDRIEST Maurizio', 32, 'Cofidis'),
  (64, 'GAUMONT Philippe', 28, 'Cofidis'),
  (65, 'GOUBERT Stéphane', 29, 'Cofidis'),
  (66, 'GWIAZDOWSKI Grzegorz', 24, 'Cofidis'),
  (67, 'PLAZA ROMERO David', 28, 'Cofidis'),
  (68, 'SAUGRAIN Cyril', 28, 'Cofidis'),
  (69, 'THIBOUT Bruno', 27, 'Cofidis'),
  
  (71, 'BOARDMAN Chris', 27, 'Credit Agricole'),
  (72, 'BOS Cyril', 28, 'Credit Agricole'),
  (73, 'MONCASSIN Frédéric', 35, 'Credit Agricole'),
  (74, 'PERRAUDEAU Olivier', 36, 'Credit Agricole'),
  (75, 'PETILLEAU Stéphane', 32, 'Credit Agricole'),
  (76, 'PRETOT Arnaud', 34, 'Credit Agricole'),
  (77, 'SEIGNEUR Eddy', 33, 'Credit Agricole'),
  (78, 'VASSEUR Cedric', 35, 'Credit Agricole'),
  (79, 'VOGELS Henk', 33, 'Credit Agricole'),
  
  (81, 'ANGUITA HINOJOSA Eleuterio', 33, 'Estepona-Brepac'),
  (82, 'CAGIGAS AMEDO Matias', 30, 'Estepona-Brepac'),
  (83, 'CEREZO PERALES Francisco Javier', 32, 'Estepona-Brepac'),
  (84, 'CERIOLI Ivan', 31, 'Estepona-Brepac'),
  (85, 'MIRANDA GONZALEZ Carmelo', 30, 'Estepona-Brepac'),
  (86, 'MORATILLA SOLERA Victor M.', 32, 'Estepona-Brepac'),
  (87, 'NIETO FERNANDEZ Germán', 32, 'Estepona-Brepac'),
  (88, 'PESCHEL Uwe', 28, 'Estepona-Brepac'),
  (89, 'VICARIO BARBERA Juan Carlos', 36, 'Estepona-Brepac'),
  
  (91, 'CHAURREAU BERNADEZ Iñigo', 28, 'Euskaltel-Euskadi'),
  (92, 'ETXEBARRIA ARANA Unai', 27, 'Euskaltel-Euskadi'),
  (93, 'FERNANDEZ BUSTINZA Bingen', 29, 'Euskaltel-Euskadi'),
  (94, 'CASTRESANA DEL VAL Ángel', 30, 'Euskaltel-Euskadi'),
  (95, 'GONZALEZ ARRIETA Ramón', 31, 'Euskaltel-Euskadi'),
  (96, 'GONZALEZ DE GALDEANO ARANZABAL Álvaro', 29, 'Euskaltel-Euskadi'),
  (97, 'GONZALEZ DE GALDEANO ARANZABAL Igor', 28, 'Euskaltel-Euskadi'),
  (98, 'LAISEKA JAIO Roberto', 25, 'Euskaltel-Euskadi'),
  (99, 'AYARZAGUEÑA URKIDI Iñaki', 29, 'Euskaltel-Euskadi'),

  (101, 'ESCARTIN COTI Fernando', 27, 'Kelme-Costa Blanca'),
  (102, 'CABELLO LUQUE Francisco', 24, 'Kelme-Costa Blanca'),
  (103, 'EDO ALSINA Ángel', 31, 'Kelme-Costa Blanca'),
  (104, 'GONZALEZ GUTIERREZ Arsenio', 33, 'Kelme-Costa Blanca'),
  (105, 'GONZALEZ PICO José Jaime', 26, 'Kelme-Costa Blanca'),
  (106, 'HERAS HERNANDEZ Roberto', 32, 'Kelme-Costa Blanca'),
  (107, 'RODRIGUEZ GARCIA José Luis', 30, 'Kelme-Costa Blanca'),
  (108, 'RUBIERA VIGIL José Luis', 29, 'Kelme-Costa Blanca'),
  (109, 'SERRANO RODRIGUEZ Marcos Antonio', 33, 'Kelme-Costa Blanca'),
  
  (111, 'AERTS Mario', 20, 'Lotto-Movistar'),
  (112, 'DE WOLF Steve', 23, 'Lotto-Movistar'),
  (113, 'DIERCKXSENS Ludo', 23, 'Lotto-Movistar'),
  (114, 'LAUKKA Joona', 27, 'Lotto-Movistar'),
  (115, 'MADOUAS Laurent', 30, 'Lotto-Movistar'),
  (116, 'MARICHAL Thierry', 26, 'Lotto-Movistar'),
  (117, 'PEERS Chris', 32, 'Lotto-Movistar'),
  (118, 'TCHMIL Andrei', 29, 'Lotto-Movistar'),
  (119, 'TETERIOUK Andrei', 22, 'Lotto-Movistar'),
  
  (121, 'VANDENBROUCKE Frank', 25, 'Mapei-Bricobi'),
  (122, 'Bramati Davide', 22, 'Mapei-Bricobi'),
  (123, 'BUGNO Gianni', 23, 'Mapei-Bricobi'),
  (124, 'DI GRANDE Giuseppe', 34, 'Mapei-Bricobi'),
  (125, 'FIGUERAS Giuliano', 29, 'Mapei-Bricobi'),
  (126, 'LANFRANCHI Paolo', 27, 'Mapei-Bricobi'),
  (127, 'MATTAN Nico', 22, 'Mapei-Bricobi'),
  (128, 'SVORADA Jan', 29, 'Mapei-Bricobi'),
  (129, 'CAMENZIND Oscar', 21, 'Mapei-Bricobi'),
  
  (131, 'JALABERT Laurent', 24, 'Once-Deustche Bank'),
  (132, 'CUESTA LOPEZ DE CASTRO Iñigo', 31, 'Once-Deustche Bank'),
  (133, 'DIAZ DE OTAZU GALARZA Luis Maria', 33, 'Once-Deustche Bank'),
  (134, 'DIAZ ZABALA Herminio', 20, 'Once-Deustche Bank'),
  (135, 'ETXEBARRIA ALKORTA David', 25, 'Once-Deustche Bank'),
  (136, 'LEANIZBARRUTIA ABAUNZA Alberto', 29, 'Once-Deustche Bank'),
  (137, 'MAURI PRAT Melcior', 22, 'Once-Deustche Bank'),
  (138, 'SIERRA AGUERRO José Roberto', 24, 'Once-Deustche Bank'),
  (139, 'ZARRABEITIA URANGA Mikel', 21, 'Once-Deustche Bank'),
  
  (141, 'GUERINI Giuseppe', 24, 'Polti'),
  (142, 'ATIENZA URENDEZ Daniel', 27, 'Polti'),
  (143, 'CREPALDI Mirko', 30, 'Polti'),
  (144, 'GUALDI Mirko', 35, 'Polti'),
  (145, 'GUIDI Fabrizio', 22, 'Polti'),
  (146, 'ROKIA Anthony', 25, 'Polti'),
  (147, 'SALVATO Cristian', 31, 'Polti'),
  (148, 'VALOTI Gianluca', 29, 'Polti'),
  (149, 'ZINETTI Mauro', 28, 'Polti'),

  (151, 'AEBERSOLD Niki', 30, 'Post Swiss Team'),
  (152, 'BEUCHAT Roger', 26, 'Post Swiss Team'),
  (153, 'BOURQUENOUD Pierre', 28, 'Post Swiss Team'),
  (154, 'CHASSOT Richard', 30, 'Post Swiss Team'),
  (155, 'HOTZ Franz', 28, 'Post Swiss Team'),
  (156, 'HUSER Rolf', 29, 'Post Swiss Team'),
  (157, 'PARADIS Daniel', 28, 'Post Swiss Team'),
  (158, 'WIRZ Guido', 34, 'Post Swiss Team'),
  (159, 'ZBERG Markus', 28, 'Post Swiss Team'),
  
  (161, 'BOOGERD Michael', 26, 'Rabobank'),
  (162, 'DEKKER Erik', 30, 'Rabobank'),
  (163, 'LOTZ Marc', 32, 'Rabobank'),
  (164, 'MCEWEN Robbie', 28, 'Rabobank'),
  (165, 'MOERENHOUT Koos', 29, 'Rabobank'),
  (166, 'SORENSEN Rolf', 24, 'Rabobank'),
  (167, 'VAN BON Leon', 28, 'Rabobank'),
  (168, 'VAN HEESWIJK Max', 28, 'Rabobank'),
  (169, 'WAUTERS Marc', 27, 'Rabobank'),
  
  (171, 'MORSCHER Harald', 27, 'Saeco'),
  (172, 'BUSCHOR Philipp', 28, 'Saeco'),
  (173, 'COMMESSO Salvatore', 35, 'Saeco'),
  (174, 'FRIGO Dario', 36, 'Saeco'),
  (175, 'KOKORINE Vitali', 32, 'Saeco'),
  (176, 'PADRNOS Pavel', 34, 'Saeco'),
  (177, 'MOOS Alexandre', 33, 'Saeco'),
  (178, 'RICH Michael', 35, 'Saeco'),
  (179, 'SAVOLDELLI Paolo', 33, 'Saeco'),
  
  (181, 'KLODEN Andreas', 33, 'Telekom'),
  (182, 'BALDINGER Dirk', 30, 'Telekom'),
  (183, 'BLAUDZUN Michael', 32, 'Telekom'),
  (184, 'DIETZ Bert', 31, 'Telekom'),
  (185, 'HUNDERTMARCK Kai', 30, 'Telekom'),
  (186, 'LOMBARDI Giovanni', 32, 'Telekom'),
  (187, 'MULLER Dirk', 32, 'Telekom'),
  (188, 'SCHAFFRATH Jan', 28, 'Telekom'),
  (189, 'TOTSCHNIG Georg', 36, 'Telekom'),
  
  (191, 'BLIJLEVENS Jeroen', 28, 'TVM-Farm Frites'),
  (192, 'DE JONGH Steven', 27, 'TVM-Farm Frites'),
  (193, 'KNAVEN Servais', 29, 'TVM-Farm Frites'),
  (194, 'LAFIS Michel', 30, 'TVM-Farm Frites'),
  (195, 'MOLLER Claus Michael', 31, 'TVM-Farm Frites'),
  (196, 'OUCHAKOV Sergei', 29, 'TVM-Farm Frites'),
  (197, 'HOFFMAN Tristan', 28, 'TVM-Farm Frites'),
  (198, 'VAN PETEGEM Peter', 25, 'TVM-Farm Frites'),
  (199, 'VOSKAMP Bart', 29, 'TVM-Farm Frites'),
  
  (201, 'ARMSTRONG Lance', 28, 'US Postal Service'),
  (202, 'EKIMOV Viatcheslav', 35, 'US Postal Service'),
  (203, 'LLANERAS ROSSELLO Juan', 27, 'US Postal Service'),
  (204, 'MEINERT NIELSEN Peter', 30, 'US Postal Service'),
  (205, 'ROBIN Jean Cyril', 32, 'US Postal Service'),
  (206, 'TEUTENBERG Sven', 33, 'US Postal Service'),
  (207, 'VANDEVELDE Christian', 30, 'US Postal Service'),
  (208, 'VAUGHTERS Jonathan', 33, 'US Postal Service'),
  (209, 'VILLATORO Anton', 34, 'US Postal Service'),
  
  (211, 'CASERO MORENO Ángel Luis', 31, 'Vitalicio Seguros'),
  (212, 'BLANCO GIL Santiago', 31, 'Vitalicio Seguros'),
  (213, 'AGGIANO Elio', 28, 'Vitalicio Seguros'),
  (214, 'CLAVERO SEBASTIAN Daniel', 33, 'Vitalicio Seguros'),
  (215, 'DOMINGUEZ DOMINGUEZ Juan Carlos', 31, 'Vitalicio Seguros'),
  (216, 'GARCIA DAPENA David', 29, 'Vitalicio Seguros'),
  (217, 'INDURAIN LARRAYA Prudencio', 37, 'Vitalicio Seguros'),
  (218, 'SMETANINE Sergei', 37, 'Vitalicio Seguros'),
  (219, 'ZINTCHENKO Andrei', 34, 'Vitalicio Seguros');

INSERT INTO `ciclistas2`.`equipo`(`nomequipo`, `director`)
VALUES ('Lotus-Festina', 'Ricardo Padacci'),
  ('Avianca-Telecom', 'José Peréz'),
  ('Banesto', 'Miguel Echevarria'),
  ('Brescialat-Liquigas', 'Pietro Armani'),
  ('Cantina Tollo', 'Luigi Petroni'),
  ('Casino', 'Jean Philip'),
  ('Cofidis', 'Pedro Txucaru'),
  ('Credit Agricole', 'Gian Luca Pacceli'),
  ('Estepona-Brepac', 'Moreno Argentin'),
  ('Eusktatel-Euskadi', 'Johan Richard'),
  ('Kelme-Costa Blanca', 'Álvaro Pino'),
  ('Lotto-Movistar', 'Suarez Cuevas'),
  ('Mapei-Bricobi', 'Juan Fernandez'),
  ('Once-Deustche Bank', 'Ettore Romano'),
  ('Polti', 'John Fidwell'),
  ('Post Swiss Team', 'Lonrenzo Sciacci'),
  ('Rabobank', 'Manuel Sainz'),
  ('Saeco', 'Piet Van Der Kruis'),
  ('Telekom', 'Morgan Reikcard'),
  ('TVM-Farm Frites', 'Steveens Henk'),
  ('Vitalicio Seguros', 'Julio Iglesias'),
  ('US Postal Service', 'Bill Gates');

INSERT INTO `ciclistas2`.`etapa`(`numetapa`, `kms`, `salida`, `llegada`, `dorsal`)
VALUES (1, 161.7, 'Córdoba', 'Córdoba', 159),
  (2, 234.6, 'Córdoba', 'Cádiz', 191),
  (3, 192.6, 'Cádiz', 'Estepona', 58),
  (4, 173.5, 'Málaga', 'Granada', 145),
  (5, 165.5, 'Olula del Río', 'Murcia', 191),
  (6, 201.5, 'Murcia', 'Xorret del Catí', 27),
  (7, 185, 'Alicante', 'Valencia', 186),
  (8, 181.5, 'Palma de Mallorca', 'Palma de Mallorca', 145),
  (9, 39.5, 'Alcudia', 'Alcudia', 21),
  (10, 199.3, 'Vic', 'Estació de Pal', 27),
  (11, 186, 'Andorra La Vella', 'Cerler', 27),
  (12, 187, 'Benasque', 'Jaca', 123),
  (13, 208.5, '	Sabiñánigo', 'Sabiñánigo', 219),
  (14, 145.5, 'Biescas', 'Zaragoza', 9),
  (15, 178.7, 'Zaragoza', 'Soria', 219),
  (16, 143.7, 'Soria', 'Laguna Negra de Neila', 27),
  (17, 188.5, 'Burgos', 'León', 9),
  (18, 230, 'León', 'Salamanca', 145),
  (19, 170.4, 'Ávila', 'Segovia', 106),
  (20, 206, 'Segovia', 'Puerto de Navacerrada', 219),
  (21, 39, 'Fuenlabrada', '	Fuenlabrada', 27),
  (22, 163, 'Madrid', 'Madrid', 159);

INSERT INTO `ciclistas2`.`lleva`(`dorsal`, `numetapa`, `código`)
VALUES (159, 1, 'MGE'),
  (159, 1, 'MPU'),
  (83, 1, 'MMO'),
  (36, 1, 'MVE'),
  
  (159, 2, 'MGE'),
  (159, 2, 'MPU'),
  (83, 2, 'MMO'),
  (36, 2, 'MVE'),
  
  (131, 3, 'MGE'),
  (58, 3, 'MPU'),
  (83, 3, 'MMO'),
  (131, 3, 'MVE'),
  
  (145, 4, 'MGE'),
  (186, 4, 'MPU'),
  (83, 4, 'MMO'),
  (131, 4, 'MVE'),
  
  (145, 5, 'MGE'),
  (191, 5, 'MPU'),
  (83, 5, 'MMO'),
  (131, 5, 'MVE'),
  
  (27, 6, 'MGE'),
  (191, 6, 'MPU'),
  (83, 6, 'MMO'),
  (131, 6, 'MVE'),
  
  (27, 7, 'MGE'),
  (191, 7, 'MPU'),
  (83, 7, 'MMO'),
  (36, 7, 'MVE'),
  
  (27, 8, 'MGE'),
  (191, 8, 'MPU'),
  (83, 8, 'MMO'),
  (36, 8, 'MVE'),
  
  (21, 9, 'MGE'),
  (191, 9, 'MPU'),
  (131, 9, 'MMO'),
  (36, 9, 'MVE'),
  
  (21, 10, 'MGE'),
  (191, 10, 'MPU'),
  (27, 10, 'MMO'),
  (36, 10, 'MVE'),
  
  (21, 11, 'MGE'),
  (191, 11, 'MPU'),
  (27, 11, 'MMO'),
  (36, 11, 'MVE'),
  
  (21, 12, 'MGE'),
  (191, 12, 'MPU'),
  (27, 12, 'MMO'),
  (36, 12, 'MVE'),
  
  (21, 13, 'MGE'),
  (145, 13, 'MPU'),
  (27, 13, 'MMO'),
  (36, 13, 'MVE'),
  
  (21, 14, 'MGE'),
  (145, 14, 'MPU'),
  (27, 14, 'MMO'),
  (36, 14, 'MVE'),
  
  (21, 15, 'MGE'),
  (145, 15, 'MPU'),
  (27, 15, 'MMO'),
  (36, 15, 'MVE'),
  
  (21, 16, 'MGE'),
  (145, 16, 'MPU'),
  (27, 16, 'MMO'),
  (36, 16, 'MVE'),
  
  (21, 17, 'MGE'),
  (145, 17, 'MPU'),
  (27, 17, 'MMO'),
  (36, 17, 'MVE'),
  
  (21, 18, 'MGE'),
  (145, 18, 'MPU'),
  (27, 18, 'MMO'),
  (36, 18, 'MVE'),
  
  (21, 19, 'MGE'),
  (145, 19, 'MPU'),
  (27, 19, 'MMO'),
  (36, 19, 'MVE'),
  
  (27, 20, 'MGE'),
  (145, 20, 'MPU'),
  (27, 20, 'MMO'),
  (36, 20, 'MVE'),
  
  (21, 21, 'MGE'),
  (145, 21, 'MPU'),
  (27, 21, 'MMO'),
  (36, 21, 'MVE'),
  
  (21, 22, 'MGE'),
  (145, 22, 'MPU'),
  (27, 22, 'MMO'),
  (36, 22, 'MVE');
  

INSERT INTO `ciclistas2`.`maillot`(`código`, `tipo`, `color`, `premio`)
VALUES ('MGE', 'General', 'Rojo', 8000000),
  ('MMO', 'Montaña', 'Lunares', 5000000),
  ('MPU', 'Puntos', 'Azul', 2000000),
  ('MVE', 'Mejor velocista', 'Verde', 1000000);

INSERT INTO `ciclistas2`.`puerto`(`nompuerto`, `altura`, `categoria`, `pendiente`, `numetapa`, `dorsal`)
VALUES ('Puerto de la Carrasqueta', 1204, '1', 4.7, 7, 27),
  ('Estación de Esquí de Cerler', 2630, 'E', 50, 11, 27),
  ('Puerto de la Cruz de Perves', 1335, '1', 6.5, 14, 101),
  ('Puerto de la Cruz verde', 1256, 'E', 4.8, 10, 101),
  ('Coll de Montaup', 1990, 'E', 5, 10, 101),
  ('Puerto de Albaida', 600, 'E', 5.5, 7, 27),
  ('Puerto de Navacerrada', 1858, '1', 5.3, 20, 131),
  ('Puerto de la Morcueraa', 1760, '2', 6.5, 20, 131),
  ('Puerto de Leon', 1115, '1', 5.6, 20, 27),
  ('Laguna Negra de Niela', 1700, '2', 5.4, 16, 27),
  ('Coll de Espina', 1407, '1', 6.6, 11, 101),
  ('Puerto de Cotos', 1830, '1', 3.4, 20, 101),
  ('Puerto de Pedro Bernardo', 1250, '1', 4.2, 18, 101),
  ('Puerto de Tibi', 723, '1', 5.4, 6, 131),
  ('Coll de sa Batalla', 576, 'E', 5.5, 4.9, 131),
  ('Sierra Nevada', 2500, 'E', 6.0, 2, 27);

-- Re-enable foreign key checks
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

-- End of script